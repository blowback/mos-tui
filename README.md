# A text user interface for mongoose-os

This is a simple terminal-based text user interface for mongoose-os.

I wrote this because on a mac, with the usb-serial adaptors I use, the supplied `mos` tool reboots the target every time you issue an RPC call, which is not helpful.

It only operates over serial port connections - for all other connection types the standard `mos` tool is much better.

It's also worth trying `mos --set-control-lines=false` if you're communicating over serial port. This works well on by linux box, but is intermittent on macos.

Oh, and it has persistent command line history.

## Command line arguments
```
Options:
  --help      Show help                                                [boolean]
  --version   Show version number                                      [boolean]
  -p, --port  serial port to use  [string] [default: "/dev/cu.usbmodem14513244"]
```

## Built-in commands

  - `reset`: reset the target device
  - `cls`: clear the console log
  - `mark`: draw a horizontal rule in the console output
  - `quit`: quit

## Special keys
  - UP ARROW: access command history (then use arrow keys, return or escape)
  - Escape (to defocus the input box) followed by Ctrl-C ==> quit


