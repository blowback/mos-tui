const blessed = require('neo-blessed');
const SerialPort = require('serialport');
const TTY = "/dev/cu.usbmodem14513244";


let screen = blessed.screen({
    smartCSR: true,
    cursor: {
        artificial: true,
        shape: 'block',
        blink: true,
        color: 'red'
    }
});

screen.title = "mos-tui";

var box = blessed.box({
    top: 0,
    left: 0,
    width: '50%',
    height: '100%-8',
    clickable: true,
    keyable: true,
    tags: true,
    border: {
        type: 'line'
    },
    style: {
        fg: 'white',
        bg: 'magenta',
        border: {
            fg: '#f0f0f0'
        },
        hover: {
            bg: 'green'
        }
    }
});

var box3 = blessed.box({
    top: 0,
    right: 0,
    width: '50%',
    height: '100%-8',
    clickable: true,
    keyable: true,
    tags: true,
    border: {
        type: 'line'
    },
    style: {
        fg: 'white',
        bg: 'green',
        border: {
            fg: '#f0f0f0'
        },
        hover: {
            bg: 'magenta'
        }
    }
});

var box2 = blessed.Textarea({
    top: '100%-8',
    left: 0,
    width: '100%',
    height: 6,
    inputOnFocus: true,
    //clickable: true,
    //keyable: true,
    //tags: true,
    //keys: true,
    border: {
        type: 'line'
    },
    style: {
        fg: 'white',
        bg: 'cyan',
        border: {
            fg: '#f0f0f0'
        },
        hover: {
            bg: 'green'
        }
    }
});

//box2.enableKeys();

screen.append(box);
screen.append(box2);
screen.append(box3);

box.on('click', function(data) {
  box.setContent('{center}Some different {red-fg}content{/red-fg}.{/center}');
  screen.render();
});

box2.on('click', () => {
    //console.log("focus");
    //box2.focus()
});

// If box is focused, handle `enter`/`return` and give us some more content.
box.key('enter', function(ch, key) {
  box.setContent('{right}Even different {black-fg}content{/black-fg}.{/right}\n');
  box.setLine(1, 'bar');
  box.insertLine(1, 'foo');
  screen.render();
});

//box2.key('enter', function(ch, key) {
    //let l = box2.getLine(box2.height - 1);
    //console.log("line: ", l);
    ////let l = box2.getLines();
    //screen.render();
    ////box.pushLine(l);
//});

//box2.on('keypress', (k) => {
    //box2.content += k;
    ////box2.setContent(box2.content + k);
    //screen.render();
//});
box2.key('enter', function(ch,k) {
    let value = this.getValue();
    //console.log(value);
    box.pushLine(value);
    this.clearValue();
    screen.render();
});



// Quit on Escape, q, or Control-C.
screen.key(['escape', 'C-c'], function(ch, key) {
  return process.exit(0);
});

// Focus our element.
box.focus();

// Render the screen.
screen.render();

//const port = new SerialPort(TTY, {
        //baudRate: 115200,
        //dataBits: 8,
        //parity: 'none',
        //stopBits: 1
    //}, (err) => {
        //if(err) {
            //throw err;
        //}

        //console.log("port opened");

        //port.on('data', (chunk) => {
            //console.log(chunk);
            //box.content += chunk;
        //});
    //});
