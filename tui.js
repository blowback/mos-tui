const os = require('os');
const fs = require('fs');
const path = require('path');
const blessed = require('neo-blessed');
const SerialPort = require('serialport');
const ReadLine = require('@serialport/parser-readline');
const TTY = "/dev/cu.usbmodem14513244";

// command line completions: key is the command, value is the timestamp of last use
let comps_path = path.resolve(os.homedir(), '.mos-tui');
let comps = {};
let comps_list = [];
let autoScroll = true;

if(fs.existsSync(comps_path)) {
    let d = fs.readFileSync(comps_path, {encoding: 'ascii'});
    comps = JSON.parse(d);
}

let argv = require('yargs')
    .options({
        'p': {
            alias: 'port',
            demandOption: false,
            default: TTY,
            describe: "serial port to use",
            type: 'string'
        }
    })
    .argv;

let screen = blessed.screen({
    smartCSR: true,
    cursor: {
        artificial: true,
        shape: 'block',
        blink: true,
        color: 'red'
    }
});

screen.title = "mos-tui";

// RPC results
var box = blessed.box({
    label: "RPC Responses",
    top: 0,
    left: 0,
    width: '50%',
    height: '100%-8',
    clickable: true,
    keyable: true,
    mouse: true,
    keys: true,
    tags: true,
    scrollable: true,
    alwaysScroll: true,
    border: {
        type: 'line'
    },
    style: {
        fg: 'white',
        bg: 'magenta',
        border: {
            fg: '#f0f0f0'
        },
    }
});

// console log
var box3 = blessed.box({
    label: "Device Logs",
    top: 0,
    right: 0,
    width: '50%',
    height: '100%-8',
    clickable: true,
    keyable: true,
    mouse: true,
    keys: true,
    tags: true,
    scrollable: true,
    alwaysScroll: true,
    scrollbar: {
        style: {
            fg: 'white',
            bg: 'green'
        },
        track: {
            fg: 'green',
            bg: 'white'
        }
    },
    border: {
        type: 'line'
    },
    style: {
        fg: 'white',
        bg: '#002200',
        border: {
            fg: '#f0f0f0'
        },
    }
});

// rpc command input area
var box2 = blessed.Textarea({
    label: "RPC Requests",
    top: '100%-8',
    left: 0,
    width: '100%',
    height: 6,
    inputOnFocus: true,
    border: {
        type: 'line'
    },
    style: {
        fg: 'white',
        bg: 'cyan',
        border: {
            fg: '#f0f0f0'
        },
    }
});

var completions = blessed.List({
    hidden: true,
    mouse: true,
    keys: true,
    items: [ 'foo', 'bar', 'baz' ],
    top: '100%-28',
    left: 0,
    width: '100%',
    height: 20,
    border: {
        type: 'line'
    },
    style: {
        fg: 'cyan',
        bg: 'white',
        border: {
            fg: '#f0f0f0'
        },
    }
});

screen.append(box);
screen.append(box2);
screen.append(box3);
screen.append(completions);

// Quit on Escape, q, or Control-C.
screen.key(['C-c'], function(ch, key) {
    return quit();
});

box3.setContent(`
******************************************************************
**   /\\/\\    ** Ant's Mongoose OS serial console, version 0.0.1 **
**   (\`')    ** Type RPC commands or {bold}'reboot'{/} or {bold}'exit'{/}         **
**    ()     ** Type {bold}esc, ctrl-c{/} to exit from any window        **
******************************************************************
`);

box2.focus();
screen.render();

const port = new SerialPort(argv.port, {
        baudRate: 115200,
        dataBits: 8,
        parity: 'none',
        stopBits: 1
    }, (err) => {
        if(err) {
            box.setContent("Can't open serial port: ", argv.port);
            return;
        }
    }
);

const parser = port.pipe(new ReadLine({
    delimiter: '\n',
    encoding: 'ascii'
}));

parser.on('data', (chunk) => {
    //console.log(chunk);
    //box3.content += chunk.toString('ascii');
    let log = false;

    try {
        let json = JSON.parse(chunk);

        if(json.hasOwnProperty('id') && json.hasOwnProperty('src')) {
            let id = json.id;
            let request = ids[id];

            if(json.result) {
                box.content += `{bold}${request.method}{/bold}\n${JSON.stringify(json.result, null, 2)}\n`;
                box.setScrollPerc(100);
                screen.render();
            }

            else if(json.error) {
                box.content += `{bold}${request.method}{/bold}{red-bg}{white-fg} ERROR {/}\n${JSON.stringify(json.error, null, 2)}\n`;
                box.setScrollPerc(100);
                screen.render();
            }
            else {
                box.content += `{bold}${request.method}{/bold}{red-bg}{white-fg}BAD RPCJSON{/}\n${JSON.stringify(json, null, 2)}\n`;
                box.setScrollPerc(100);
                screen.render();
            }
        } else {
            log = true;
        }
    } catch(err) {
        log = true;
    }

    if(log) {
        box3.content += chunk + "\n";

        if(autoScroll) {
            box3.setScrollPerc(100);
        }

        screen.render();
    }
});

box2.key('enter', function(ch,k) {
    let value = this.getValue();
    //console.log(value);
    //box.pushLine(value);
    sendRPCJson(port, value);
    this.clearValue();
    screen.render();
});

box2.key('up', function() {
    box2.cancel();
    comps_list = Object.keys(comps).sort((a, b) => comps[a] - comps[b]);
    completions.setItems(comps_list);
    completions.show();
    completions.focus();
    screen.render();
});

completions.on('select', function(s, idx) {
    //console.log("selected ", idx);
    let item = comps_list[idx];
    //console.log("inserting ", item);
    box2.setValue(item);
    completions.hide();
    box2.focus();
    screen.render();
});

completions.on('cancel', function() {
    completions.hide();
    box2.focus();
    screen.render();
});

//completions.key('escape', (e) => {
    //completions.hide();
    //box2.focus();
    //screen.render();
    //return false;
//});

function quit() {
    fs.writeFileSync(comps_path, JSON.stringify(comps));
    return process.exit(0);
}

let ids = {};

function nextId() {
    if(nextId.id === undefined) {
        nextId.id = 1;
    }

    return nextId.id++;
}

function sendRPCJson(port, command) {

    let spc = command.indexOf(' ');

    if(spc >= 0) {
        var method = command.substr(0, spc);
        var params = JSON.parse(command.substr(spc + 1));
    } else {
        var method = command.trim();
    }

    if(method === 'reset') {
        port.set({dtr: false});
        setTimeout(() => port.set({dtr:true}), 500);
        return;
    }

    else if(method === 'cls') {
        box3.setContent('');
        screen.render();
        return;
    }

    else if(method === 'mark') {
        box3.pushLine('#####################################\n');
        screen.render();
        return;
    }

    else if(method === 'quit') {
        quit();
    }

    else if(method === 'noscroll') {
        autoScroll = false;
        return;
    }

    else if(method === 'scroll') {
        autoScroll = true;
        return;
    }

    else if(method === '') {
        return;
    }

    // add to completions list
    comps[command.trim()] = Date.now();


    let id = nextId();

    let rpcjson = {
        jsonrpc: "2.0",
        method,
        params,
        id
    };

    ids[id] = rpcjson;

    let data = JSON.stringify(rpcjson) + "\r\n";
    //console.log("sending ", data);

    port.write(data);
}
